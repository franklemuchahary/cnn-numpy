# README #

### Convolutional Neural Networks from scratch without any Neural Network Libraries

This small project was an attempt to understand the intricacies and the mathematics behind the backpropagation for 
convolutional layers in a better way.

A very crude version of CNN was implemented using mostly just Numpy and tested on a very small subset with just 2 classes
of the MNIST dataset. A 97% accuracy score was obtained on the test set of the subset of MNIST dataset used.

The network has one convolutional layer followed by a tanh activation layer. This is followed by a single fully connected layer
which in turn is followed by a sigmoid actvation layer. The network was trained using Stochastic Gradient Descent. There are 
also other activation options like ReLU.



